<?php

$bmw = [
    'model' => 'X5',
    'speed' => '120',
    'doors' => '5',
    'year' => '2015'
];
$toyota = [
    'model' => 'T5',
    'speed' => '230',
    'doors' => '8',
    'year' => '2017'
];
$opel = [
    'model' => 'P5',
    'speed' => '999',
    'doors' => '12',
    'year' => '2025'
];
$carsArr['bmw'] = $bmw;
$carsArr['toyota'] = $toyota;
$carsArr['opel'] = $opel;
foreach ($carsArr as $name => $car) {
    echo "CAR $name<br> 
    {$car['model']} - {$car['speed']} - {$car['doors']} - {$car['year']}<br>";
}